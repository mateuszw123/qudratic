package com.demo.springboot.dto;

public class ParametsDto {

    private Double a;
    private Double b;
    private Double c;

    public ParametsDto(Double a) {

    }

    public Double getA() {
        return a;
    }

    public Double getB() {
        return b;
    }

    public Double getC() {
        return c;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "ParametsDto{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                '}';
    }
}
