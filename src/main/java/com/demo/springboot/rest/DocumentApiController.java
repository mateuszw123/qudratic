package com.demo.springboot.rest;

import com.demo.springboot.dto.ParametsDto;
import com.demo.springboot.dto.ResultDto;
import com.demo.springboot.service.QudraticFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

public class DocumentApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentApiController.class);

    public DocumentApiController(QudraticFunction qudraticFunction) {
        this.qudraticFunction = qudraticFunction;
    }

    private QudraticFunction qudraticFunction;

    @GetMapping("/api/math/quadratic-function")
    public ResponseEntity<ResultDto> calculateF(@RequestParam Double a,
                                                @RequestParam Double b,
                                                @RequestParam Double c){

        LOG.info("---a: {}", a);
        LOG.info("---a: {}", b);
        LOG.info("---a: {}", c);

        return ResponseEntity.ok(qudraticFunction.calculateF(a,b,c));
    }

    @PostMapping("/api/math/quadratic-function")
    public ResponseEntity<ResultDto> calculateF(@RequestBody ParametsDto parametsDto){

        LOG.info("------ params: {}", parametsDto.toString())

        return ResponseEntity
                .ok(qudraticFunction.calculateF(parametsDto.getA(), parametsDto.getB(), parametsDto.getC()));
    }

}
